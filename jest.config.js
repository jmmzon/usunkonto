module.exports = {
    moduleFileExtensions: [
        'js',
        'json',
        'ts',
        'tsx',
    ],
    transform: {
        '^.+\\.(ts|tsx)$': 'ts-jest',
    },
    globals: {
        'ts-jest': {
            tsConfig: 'tsconfig.json',
        },
    },
    testMatch: [
        '<rootDir>/**/*.test.+(ts|tsx|js)',
    ],
    testPathIgnorePatterns: [
        '/node_modules/',
        '/dist/',
        '/lib/',
    ],
    preset: 'ts-jest',
};

