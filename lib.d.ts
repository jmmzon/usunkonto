import {UserInterface} from "./src/server/models/User";

declare global {
    namespace Express {
        interface Request {
            user?: UserInterface
        }
    }
}
