import NuxtConfiguration from '@nuxt/config';

const config: NuxtConfiguration = {
    // modules: ['nuxt-ts'],
    srcDir: 'src/client'
};

export default config;

