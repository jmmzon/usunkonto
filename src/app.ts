import * as bodyParser from 'body-parser';
import {Server} from "@overnightjs/core";
import 'express-async-errors';
import {celebrate, errors} from 'celebrate';
import {RegistrationController} from "@server/modules/registration/RegistrationController";
import {AuthorizationController} from "@server/modules/authorization/AuthorizationController";
import responseErrorHandlerMiddleware from "@server/middlewares/responseErrorHandlerMiddleware";
import {PrivateMessagesController} from "@server/modules/privateMessages/PrivateMessagesController";
import {BlackListController} from "@server/modules/blacklist/BlackListController";
import {MicroblogController} from "@server/modules/microblog/microblogController";
import unauthorizedErrorHandlerMiddleware from "./server/middlewares/unauthorizedErrorHandlerMiddleware";
import {NotificationsController} from "@server/modules/notifications/notificationsController";
import nuxtConfig from '../nuxt.config';
import {Builder, Nuxt} from 'nuxt';

export class App extends Server {
    constructor() {
        super();
        this.setupRequestMiddlewares();
        this.setupControllers();
        this.setupResponseMiddlewares();
    }

    private setupRequestMiddlewares():void {
        this.app.use(bodyParser.json());
        this.app.use(celebrate({headers: {}}, {allowUnknown: true}));
    }

    private setupResponseMiddlewares(): void {
        this.app.use(errors());
        this.app.use(responseErrorHandlerMiddleware);
        this.app.use(unauthorizedErrorHandlerMiddleware);
    }

    private setupControllers():void {
        const authorizationController = new AuthorizationController();
        const registrationController = new RegistrationController();
        const privateMessagesController = new PrivateMessagesController();
        const blackListController = new BlackListController();
        const microblogController = new MicroblogController();
        const notificationsController = new NotificationsController();

        super.addControllers([
            authorizationController,
            registrationController,
            privateMessagesController,
            blackListController,
            microblogController,
            notificationsController
        ]);
    }

    public setupNuxt() {
        const nuxt: Nuxt = new Nuxt(nuxtConfig);
        this.app.use(nuxt.render);

        return new Builder(nuxt).build();
    }

    public start(port: number = 3000) {
        this.app.listen(port);
    }

}
