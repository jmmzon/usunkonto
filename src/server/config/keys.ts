import {resolve} from 'path';

export const PRIVATE_KEY_FILENAME = 'jwt.private';
export const PUBLIC_KEY_FILENAME = 'jwt.pub';

export const PRIVATE_KEY_PATH = resolve(PRIVATE_KEY_FILENAME);
export const PUBLIC_KEY_PATH  = resolve(PUBLIC_KEY_FILENAME);