import {ResponseError} from "./ResponseError";

export class AuthorizationError extends ResponseError {
    constructor(message: string, details?) {
        super(401, message, details);
    }
}