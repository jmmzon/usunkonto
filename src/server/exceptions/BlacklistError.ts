import {ResponseError} from "./ResponseError";

export default class BlacklistError extends ResponseError {
    constructor() {
        super(400, 'You are blacklisted or user has blocked pw');
    }
}