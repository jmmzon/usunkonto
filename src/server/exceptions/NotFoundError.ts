import {ResponseError} from "./ResponseError";

export class NotFoundError extends ResponseError {
   constructor(message, details?) {
       super(404, message, details);
   }
}