import {ResponseError} from "@server/exceptions/ResponseError";

export class PermissionError extends ResponseError {
    constructor(message) {
        super(403, message);
    }
}