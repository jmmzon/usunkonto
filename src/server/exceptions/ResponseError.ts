export class ResponseError extends Error {
    status: number;
    message: string;
    details: string;

    constructor(status: number, message:string = '', details?:any) {
        super(message);
        this.name = 'ResponseError';
        this.status = status;
        this.message = message;
        this.details = details;
    }
}

export class ValidationError extends ResponseError {
    constructor(message, details?) {
        super(400, message, details);
    }
}