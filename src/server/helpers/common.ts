export interface HasIDInterface {
    id: number;
}

export function getID(obj: HasIDInterface):number {
    return obj.id;
}