import {UserSettings} from "@server/models/UserSettings";

export function convertSettingValue(setting: UserSettings):any {
    switch (setting.attribute.type) {
        case 'boolean':
            return setting.value === 'true';
        case 'integer':
            return parseInt(setting.value);
        default:
            return setting.value;
    }
}