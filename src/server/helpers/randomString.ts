import {randomBytes} from 'crypto';

export default function randomString(length: number):string {
    return randomBytes(length).toString('hex');
}