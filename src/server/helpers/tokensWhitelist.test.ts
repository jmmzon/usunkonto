import {TokensWhitelist} from "./tokensWhitelist";

class TokensWhitelistSuite extends TokensWhitelist {
    static reset() {
        TokensWhitelist.instance = null;
    }
}







describe('tokens whitelist test suit', () => {
   beforeEach(() => {
       TokensWhitelistSuite.reset();
   });


   it('should return user has not given token', async () => {
      const tokensWhitelist = TokensWhitelist.getInstance(() => ({1: ['TOKEN1']}));
      expect(await tokensWhitelist.hasUserToken(2, 'TOKEN2')).toEqual(false);
      expect(await tokensWhitelist.hasUserToken(1, 'TOKEN10')).toEqual(false);
      expect(await tokensWhitelist.hasUserToken(1, 'TOKEN1')).toEqual(true);
   });


   it('should add user token', async () => {
       const tokensWhitelist = TokensWhitelist.getInstance();
       await tokensWhitelist.addUserToken(1, 'TOKEN1');

       expect(await tokensWhitelist.hasUserToken(1, 'TOKEN1')).toEqual(true);
       expect(await tokensWhitelist.hasUserToken(1, 'TOKEN2')).toEqual(false);
   });

    it('should remove user token', async () => {
      const tokensWhitelist = TokensWhitelist.getInstance(() => ({1: ['TOKEN1', 'TOKEN2']}));
      await tokensWhitelist.removeUserToken(1, 'TOKEN1');
      expect(await tokensWhitelist.hasUserToken(1, 'TOKEN1')).toEqual(false);
      expect(await tokensWhitelist.hasUserToken(1, 'TOKEN2')).toEqual(true);
    });

    it('should remove all user tokens', async () => {
        const tokensWhitelist = TokensWhitelist.getInstance(() => ({1: ['TOKEN1', 'TOKEN2']}));
        await tokensWhitelist.removeAllUserTokens(1);
        expect(await tokensWhitelist.hasUserToken(1, 'TOKEN1')).toEqual(false);
        expect(await tokensWhitelist.hasUserToken(1, 'TOKEN2')).toEqual(false);
    });




});
