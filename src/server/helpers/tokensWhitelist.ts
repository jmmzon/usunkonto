import * as R from 'ramda';

interface UserTokens {
    [id: number]: string[];
}

//TODO: use redis
export class TokensWhitelist {
    protected userTokens: UserTokens = {};
    protected static instance?: TokensWhitelist = null;

    protected constructor(initial) {
        this.userTokens = initial && initial() || {};
    }

    static getInstance(initial?):TokensWhitelist {
        if(!this.instance) {
            this.instance = new TokensWhitelist(initial);
        }
        return this.instance;
    }

    async addUserToken(userID: number, token: string): Promise<void> {
        this.userTokens[userID] = this.userTokens[userID] || [];
        this.userTokens[userID].push(token);
    }

    async removeUserToken(userID: number, token: string): Promise<void> {
        if(!this.userTokens[userID]) {
            return;
        }

        const notEqualToken = R.unary(R.pipe(R.equals(token), R.not));
        this.userTokens[userID] = this.userTokens[userID].filter(notEqualToken);
    }

    async removeAllUserTokens(userID: number): Promise<void> {
        this.userTokens[userID] = [];
    }

    async hasUserToken(userID:number, token:string):Promise<boolean> {
        // return false; //TODO: uncomment below code, if redis will be implemented
        return this.userTokens[userID] !== undefined && this.userTokens[userID].indexOf(token) >= 0;
    }

    isRevoked(req, payload, done):Promise<void> {
        const userID: number = payload.id;
        const token:string = req.headers.authorization.substring('Bearer '.length);

        return done(null, false);

        // return this.hasUserToken(userID, token).then(hasUserToken => {
        //     return done(null, hasUserToken);
        // });
    }

}