import keyBy from "./keyBy";

interface User {
    id: number;
    firstName: string;
    lastName: string;
}

describe('keyBy test suite', () => {
    const collection:User[] = [
        {
            id: 1,
            firstName: "Jan",
            lastName: "Nowak"
        },
        {
            id: 2,
            firstName: "Anna",
            lastName: "Nowak"
        }
    ];

    it('should return object with keys make from id and matching values', () => {
        expect(keyBy(collection, 'id')).toEqual({
            1: collection[0],
            2: collection[1]
        });
    });

    it('should return object with one key make from lastName and second collection value', () => {
        expect(keyBy(collection, 'lastName')).toEqual({
            "Nowak": collection[1]
        });
    });


    it('should return object with keys make from id and matching values', () => {
        expect(keyBy(collection, 'firstName')).toEqual({
            "Jan": collection[0],
            "Anna": collection[1]
        });
    });

});