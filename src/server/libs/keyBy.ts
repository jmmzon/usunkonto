export interface Dictionary<T> {
    [id: string]: T;
}

export default function keyBy<T>(collection: T[], keyBy: keyof T):Dictionary<T> {
    return collection.reduce((acc: Dictionary<T>, item: T) => {
        acc[item[keyBy as string]] = item;
        return acc;
    }, {})
}