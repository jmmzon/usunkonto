import {readFileSync} from 'fs';
import * as jwt from 'express-jwt';
import {PUBLIC_KEY_PATH} from "@server/config/keys";
import {TokensWhitelist} from '@server/helpers/tokensWhitelist';

const publicKey = readFileSync(PUBLIC_KEY_PATH);
export default jwt({
    secret: publicKey,
    isRevoked: TokensWhitelist.getInstance().isRevoked
});

export const getUser = jwt({
    secret: publicKey,
    credentialsRequired: false,
    isRevoked: TokensWhitelist.getInstance().isRevoked
});