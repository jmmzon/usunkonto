import {ResponseError, ValidationError} from "@server/exceptions/ResponseError";
import {NextFunction, Request, Response} from "express";

export default function responseErrorHandlerMiddleware(error: ValidationError, request: Request, response: Response, next: NextFunction) {
    if(error.name !== 'ResponseError') {
        return next(error);
    }

    const status = error.status || 500;
    const message = error.message || 'Something went wrong';
    const details = JSON.stringify(error.details);

    response.status(status).send({
        status,
        message,
        details
    });
}