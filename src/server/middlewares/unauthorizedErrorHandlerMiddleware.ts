import {NextFunction, Request, Response} from "express";

export default function unauthorizedErrorHandlerMiddleware(error: Error, request: Request, response: Response, next: NextFunction) {
    if(error.name !== 'UnauthorizedError') {
        return next(error);
    }

    return response.status(401).send(error.message);
}
