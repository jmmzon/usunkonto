import {
    Column,
    CreateDateColumn,
    Entity,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {User} from "./User";
import {Tag} from "./Tag";
import {MAX_POST_EDIT_ANSWERS, MAX_POST_EDIT_TIME} from "@server/config/app";

export interface PostInterface {
    id?: number;
    text?: string;
    mentionedUsers?: User[];
    tags?: Tag[];
    answerTo?: PostInterface;
}

export interface PostCommentInterface extends PostInterface{

}

//TODO: rewritten to tree structure, so more than one level comments can be maked
@Entity()
export class Post implements PostInterface{
    async canBeEdited() {
        const editTimePassed = Date.now() - Date.parse(this.createdAt) > MAX_POST_EDIT_TIME;
        const tooManyAnswers = this.answers.length > MAX_POST_EDIT_ANSWERS;

        return !editTimePassed && !tooManyAnswers;
    }


    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    text: string;

    @ManyToOne(type => User)
    user: User;

    @ManyToMany(type => User)
    @JoinTable()
    mentionedUsers: User[];

    @ManyToMany(type => Tag)
    @JoinTable()
    tags: Tag[];

    @OneToMany(type => Post, post => post.answerTo)
    answers: Post[];

    @ManyToOne(type => Post, post => post.answers, {nullable: true})
    answerTo: Post;

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn()
    updatedAt: string;
}