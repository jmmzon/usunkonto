import {Entity, ManyToOne} from "typeorm";
import {Vote} from "./Vote";
import {Post} from "./Post";

@Entity()
export class PostVote extends Vote {
    @ManyToOne(type => Post)
    post: Post;
}