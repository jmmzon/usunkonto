import {User, UserInterface} from "./User";
import {
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";

export interface PrivateMessageInterface {
    sender: UserInterface;
    receiver: UserInterface;
    message: string;
}


@Entity()
export class PrivateMessage implements PrivateMessageInterface {
    @PrimaryGeneratedColumn()
    id: number;


    @ManyToOne(type => User, user => user.sendedMessages)
    @JoinColumn({name: "senderId"})
    sender: User;

    @ManyToOne(type => User, user => user.receivedMessages, {onDelete: "CASCADE"})
    @JoinColumn({name: "receiverId"})
    receiver: User;


    @Column()
    message: string;

    @Column()
    seen: boolean;

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn()
    updatedAt: string;

}