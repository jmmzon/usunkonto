import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class SettingsAttribute {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({nullable: true})
    type: string;

    @Column({nullable: true})
    description: string;
}