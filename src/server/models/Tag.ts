import {Column, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./User";

interface TagInterface {
    name: string
}

@Entity()
export class Tag implements TagInterface {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToOne(type => User, user => user.authorTags, {nullable: true})
    author: Promise<User>;

    @ManyToMany(type => User, user => user.observedTags)
    observedBy: User[];
}