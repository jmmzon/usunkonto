import {Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";
import {User} from "./User";
import {Tag} from "./Tag";
import {Post} from "@server/models/Post";

@Entity()
export class TagNotification {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User)
    receiver: User;

    @ManyToOne(type => Tag)
    tag: Tag;

    @Column({
        default: false
    })
    seen: boolean;

    @ManyToOne(type => Post, {nullable: true})
    post?: Post;

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn()
    updatedAt: string;
}