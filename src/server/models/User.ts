import {
    AfterLoad,
    Column,
    CreateDateColumn,
    Entity,
    JoinTable,
    ManyToMany,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {UserAccountActivation} from "./UserAccountActivation";
import {Tag} from "./Tag";
import {PrivateMessage} from "./PrivateMessage";
import {TagNotification} from "./TagNotification";
import {UserSettings} from "./UserSettings";
import * as bcrypt from "bcrypt";
import * as dayjs from 'dayjs';
import {getRepository} from "@server/models";
import {convertSettingValue} from "@server/helpers/convertSettingValues";
import {UserMentionNotification} from "@server/models/UserMentionNotification";


export interface UserInterface {
    id?: number;
    username?: string;
    firstName?: string;
    lastName?: string;
    password?: string;
    email?: string;
}

export enum UserStatus {
    ACTIVE = 'active',
    INACTIVE = 'inactive',
    BANNED = 'banned'
}

@Entity()
export class User implements UserInterface {

    public async setPassword(password: string): Promise<User> {
        this._password = await bcrypt.hash(password, 2048);
        return this;
    }

    public async isValidPassword(password: string): Promise<boolean> {
        return await bcrypt.compare(password, this._password);
    }

    public setStatus(newStatus: UserStatus) {
        this.status = newStatus;
    }

    @AfterLoad()
    reputation() {
        return  dayjs().diff(this.createdAt, 'month') > 1 ? 'pomarańczka' : 'zielonka'
    }

    /**
     * Return user settings value for specific attribute
     *
     * @param {string} attribute
     */
    public async getSettingValue(attribute: string):Promise<any> {
        const userSettingsRepository = await getRepository(UserSettings);

        const setting = await userSettingsRepository.createQueryBuilder('userSettings')
            .where('userSettings.user = :user', {user: this.id})
            .leftJoinAndSelect('userSettings.attribute', 'attribute', 'attribute.name = :attribute', {attribute})
            .getOne();

        return setting && convertSettingValue(setting);
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        // type: 'enum',
        // enum: UserStatus,
        default: 'inactive',
    })
    status: string;

    @Column()
    username: string;

    @Column({nullable: true})
    firstName: string;

    @Column({nullable: true})
    lastName: string;

    @Column()
    email: string;

    @Column({length: 64, select: false})
    protected _password: string;

    @OneToOne(type => UserAccountActivation, activationCode => activationCode.user)
    activationCode: Promise<UserAccountActivation>;

    @OneToMany(type => Tag, tag => tag.author)
    authorTags: Tag[];

    @ManyToMany(type => Tag, tag => tag.observedBy)
    @JoinTable()
    observedTags: Tag[];

    @ManyToMany(type => User, user => user.observedBy)
    @JoinTable()
    observedUsers: User[];

    @ManyToMany(type => User, user => user.observedUsers)
    observedBy: User[];

    @ManyToMany(type => User, user => user.blacklistedBy)
    @JoinTable()
    blacklistedUsers: User[];

    @ManyToMany(type => User, user => user.blacklistedUsers)
    blacklistedBy: Promise<User[]>;

    @OneToMany(type => PrivateMessage, message => message.receiver)
    receivedMessages: Promise<PrivateMessage>;

    @OneToMany(type => PrivateMessage, message => message.sender)
    sendedMessages: Promise<PrivateMessage>;

    @OneToMany(type => TagNotification, tagNotification => tagNotification.receiver)
    tagsNofifications: Promise<TagNotification>;

    @OneToMany(type => UserSettings, userSettings => userSettings.user)
    settings: UserSettings[];

    @OneToMany(type => UserMentionNotification, mentionNotification => mentionNotification.mentioned)
    mentionedBy: UserMentionNotification[];

    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn()
    updatedAt: string;
}
