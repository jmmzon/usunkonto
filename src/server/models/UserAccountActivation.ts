import {
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {User} from "./User";

@Entity()
export class UserAccountActivation {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    activationCode?: string;

    @OneToOne(type => User, user => user.activationCode, {eager: true})
    @JoinColumn()
    user: User;


    @CreateDateColumn()
    createdAt: string;

    @UpdateDateColumn()
    updatedAt: string;
}