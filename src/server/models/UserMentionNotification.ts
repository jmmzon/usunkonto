import {Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {User, UserInterface} from "@server/models/User";
import {Post, PostInterface} from "@server/models/Post";

export interface UserMentionNotificationInterface {
    id?: number;
    caller?: UserInterface;
    mentioned?: UserInterface;
    post?: PostInterface;
}

@Entity()
export class UserMentionNotification implements UserMentionNotificationInterface{

    @PrimaryGeneratedColumn()
    id: number;

    @Column({default: false})
    seen: boolean;

    @ManyToOne(type => User)
    caller: User;

    @ManyToOne(type => User)
    mentioned: User;

    @ManyToOne(type => Post, {nullable: true})
    post?: Post;

    @CreateDateColumn()
    createdAt: string;
}