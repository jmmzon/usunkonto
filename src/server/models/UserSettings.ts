import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./User";
import {SettingsAttribute} from "./SettingsAttribute";

@Entity()
export class UserSettings {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User)
    user: User;

    @ManyToOne(type => SettingsAttribute, {eager: true})
    attribute: SettingsAttribute;

    @Column()
    value: string;
}