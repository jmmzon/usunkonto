import {Column, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./User";

export abstract class Vote {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User)
    user: User;

    @Column()
    strength: number
}