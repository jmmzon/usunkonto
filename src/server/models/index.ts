import "reflect-metadata";
import {Connection, createConnection, EntitySchema, ObjectType, Repository} from "typeorm";
import {User} from './User';
import {UserAccountActivation} from "./UserAccountActivation";
import {Tag} from "./Tag";
import {TagNotification} from "./TagNotification";
import {PrivateMessage} from "./PrivateMessage";
import {UserSettings} from "./UserSettings";
import {SettingsAttribute} from "./SettingsAttribute";
import {Post} from "./Post";
import {PostVote} from "./PostVote";
import {UserMentionNotification} from "@server/models/UserMentionNotification";

const connection: Promise<Connection> = createConnection({
    type: 'sqlite',
    database: 'database.sqlite',
    synchronize: true,
    entities: [User, UserAccountActivation, Tag, TagNotification, PrivateMessage, UserSettings, SettingsAttribute, Post, PostVote, UserMentionNotification]
});

export default connection;

export function getConnection() {
    return connection;
}

export async function getRepository<Entity>(enitity: ObjectType<Entity>):Promise<Repository<Entity>> {
    return connection.then((c) => c.getRepository(enitity));
}

export async function getRepositories(entities) {
    return connection.then((c: Connection) => Promise.all(entities.map((e: EntitySchema) => c.getRepository(e))))
}
