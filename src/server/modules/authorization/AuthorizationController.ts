import {Request, Response} from 'express';
import {Controller, Middleware, Post} from "@overnightjs/core";
import {
    assertAccountIsActivated,
    assertCredentialsAreValid,
    validateUserLoginRequest,
    validateUserLogoutRequest
} from "./AuthorizationValidations";
import {JWTToken} from "./types";
import {createToken} from "./AuthorizationDLA";
import {User} from "@server/models/User";
import requireAuthorization from "@server/middlewares/requireAuthorization";
import {TokensWhitelist} from "@server/helpers/tokensWhitelist";

@Controller('api/authorization')
export class AuthorizationController {
    @Post('login')
    @Middleware([validateUserLoginRequest])
    private async postLogin(req: Request, res: Response): Promise<any> {
        const user: User = await assertCredentialsAreValid(req.body);
        await assertAccountIsActivated(user);

        const token: JWTToken = await createToken(user);
        await TokensWhitelist.getInstance().addUserToken(user.id, token as string);

        res.send(token);
    }

    @Post('logout')
    @Middleware([
        requireAuthorization,
        validateUserLogoutRequest
    ])
    private async postLogout(req: Request, res: Response): Promise<void> {
        await TokensWhitelist.getInstance().removeUserToken(req.user.id, req.headers.authorization.substring('Bearer '.length));
        res.send();
    }

    @Post('logout/everywhere')
    @Middleware([
        requireAuthorization,
        validateUserLogoutRequest
    ])
    private async postLogoutEverywhere(req: Request, res: Response):Promise<void> {
        await TokensWhitelist.getInstance().removeAllUserTokens(req.user.id);
        res.send();
    }
}