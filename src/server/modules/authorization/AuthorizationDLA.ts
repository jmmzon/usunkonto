import {JWTToken, TokenData, UserCredentials} from "./types";
import connection from '@server/models';
import {User} from "@server/models/User";
import {Connection, Repository} from "typeorm";
import * as jwt from 'jsonwebtoken';
import {PRIVATE_KEY_PATH} from "@server/config/keys";
import * as fs from 'fs';
import {promisify} from "util";


export const readFile = promisify(fs.readFile.bind(fs));
export const generateToken = (tokenData, privateKey): Promise<JWTToken> =>
    new Promise((resolve, reject) => {
        jwt.sign(tokenData, privateKey, {algorithm: 'RS256'}, (err, token: JWTToken) => err ? reject(err) : resolve(token));
    });


export async function checkIfUserCredentialsAreValid(credentials: UserCredentials): Promise<User | boolean> {
    const db: Connection = await connection;
    const userRepository: Repository<User> = db.getRepository(User);

    const user: User = await userRepository.createQueryBuilder('user')
        .where('user.username = :username', credentials)
        .addSelect('user._password')
        .leftJoinAndSelect('user.activationCode', 'activationCode')
        .getOne();


    if (!user) {
        return false;
    }

    if (!await user.isValidPassword(credentials.password)) {
        return false;
    }

    return user;
}



export async function createToken(user: User): Promise<JWTToken> {
    const tokenData: TokenData = {
        id: String(user.id),
        username: user.username,
    };
    const privateKey = await readFile(PRIVATE_KEY_PATH);
    return generateToken(tokenData, privateKey);
}
