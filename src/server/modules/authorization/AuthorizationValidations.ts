import {celebrate, Joi} from 'celebrate';
import {UserCredentials} from "./types";
import {checkIfUserCredentialsAreValid} from "./AuthorizationDLA";
import {ValidationError} from "@server/exceptions/ResponseError";
import {User, UserStatus} from "@server/models/User";
import {AuthorizationError} from "@server/exceptions/AuthorizationError";

export const validateUserLoginRequest = celebrate({
   body: {
       username: Joi.string().required(),
       password: Joi.string().required()
   },
});


export const validateUserLogoutRequest = celebrate({
   headers: {
       authorization: Joi.string().required()
   },
}, {allowUnknown: true});

export async function assertCredentialsAreValid(credentials: UserCredentials):Promise<User> {
    const user:User = await checkIfUserCredentialsAreValid(credentials) as User;

    if(!user) {
        throw new ValidationError('Invalid credentials');
    }

    return user;
}

export async function  assertAccountIsActivated(user: User) {
    if(user.status === UserStatus.INACTIVE as string) {
        throw new AuthorizationError('Account not activated');
    }
}