export interface UserCredentials {
    username: string;
    password: string;
}

export interface JWTToken extends String{}

export interface TokenData {
    id: string;
    username: string;
}