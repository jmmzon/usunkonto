import {Controller, Delete, Get, Middleware, Post} from "@overnightjs/core";
import {addUserToBlackList, getBlacklistedUsers, removeUserFromBlacklist} from "./BlacklistDLA";
import {Request, Response} from "express";
import {assertUserExists} from "../users/usersValidations";
import requireAuthorization from "@server/middlewares/requireAuthorization";
import {validateAddAndRemoveBlackListUser} from "./BlackListValidaiton";

@Controller('api/blacklist')
export class BlackListController {
    @Get('users')
    @Middleware([
        requireAuthorization
    ])
    private async getBlacklistedUsers(req: Request, res: Response) {
        const {userDB} = await getBlacklistedUsers(req.user, req.user);
        return res.send(userDB.blacklistedUsers);
    }

    @Get('tags')
    @Middleware([
        requireAuthorization
    ])
    private getBlacklistedTags() {

    }

    @Get('domains')
    @Middleware([
        requireAuthorization
    ])
    private getBlacklistedDomains() {

    }

    @Post('user/:userID')
    @Middleware([
        requireAuthorization,
        validateAddAndRemoveBlackListUser
    ])
    private async postBlacklistUser(req: Request, res: Response) {
        await assertUserExists({id: req.params.userID});
        await addUserToBlackList(req.user, {id: req.params.userID});
        return res.send();
    }

    @Post('tag/:userID')
    @Middleware([
        requireAuthorization
    ])
    private postBlacklistTag() {

    }

    @Post('domain')
    @Middleware([
        requireAuthorization
    ])
    private postBlacklistedDomain() {

    }

    @Delete('user/:userID')
    @Middleware([
        requireAuthorization,
        validateAddAndRemoveBlackListUser
    ])
    private async deleteBlacklistedUser(req: Request, res: Response) {
        await assertUserExists({id: req.params.userID});
        await removeUserFromBlacklist(req.user, {id: req.params.userID});
        return res.send();
    }

    @Delete('tag/:tagID')
    @Middleware([
        requireAuthorization
    ])
    private deleteBlacklistedTag() {

    }

    @Delete('domain')
    @Middleware([
        requireAuthorization
    ])
    private deleteBlacklistedDomain() {

    }
}