import {celebrate, Joi} from "celebrate";

export const validateAddAndRemoveBlackListUser = celebrate({
    params: {
        userID: Joi.number().required()
    }
});