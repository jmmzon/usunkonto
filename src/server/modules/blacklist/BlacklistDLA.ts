import {User, UserInterface} from "@server/models/User";
import {getRepository} from "@server/models";
import {In} from "typeorm";
import keyBy from "@server/libs/keyBy";


export async function hasSecondUsersBlacklistedFirstUser(firstUser: User, secondUser: User) {
    const blacklistedBy = await firstUser.blacklistedBy;
    return blacklistedBy.find(({id}) => secondUser.id == id);
}

interface blacklisted {
    userDB: User,
    userToBlacklistDB: User
}

export async function getBlacklistedUsers(user: UserInterface, blacklistedUser: UserInterface):Promise<blacklisted> {
    const userRepository = await getRepository(User);
    const {
        [user.id]: userDB,
        [blacklistedUser.id]: userToBlacklistDB
    } = keyBy(await userRepository.find({
        where: {
            id: In([user.id, blacklistedUser.id])
        },
        relations: ['blacklistedUsers']
    }), 'id');

    return {userDB, userToBlacklistDB};
}



export async function addUserToBlackList(user: UserInterface, blacklistedUser: UserInterface) {
    const userRepository = await getRepository(User);

    const {
        userDB,
        userToBlacklistDB
    } = await getBlacklistedUsers(user, blacklistedUser);

    if(userDB.blacklistedUsers.indexOf(userToBlacklistDB) < 0) {
        userDB.blacklistedUsers.push(userToBlacklistDB);
    }

    return await userRepository.save(userDB);
}

export async function removeUserFromBlacklist(user: UserInterface, blacklistedUser: UserInterface) {
    const userRepository = await getRepository(User);

    const {
        userDB,
    } = await getBlacklistedUsers(user, blacklistedUser);

    userDB.blacklistedUsers = userDB.blacklistedUsers.filter(({id}) => id != blacklistedUser.id);

    return await userRepository.save(userDB);
}