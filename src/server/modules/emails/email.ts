export interface Email {
    subject: string;
    message: string;
    receiver: string;
}

