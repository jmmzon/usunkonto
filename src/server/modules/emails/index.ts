import * as nodemailer from 'nodemailer';
import * as config from "../../config";
import {Email} from "./email";
import * as util from 'util';


const transporter = nodemailer.createTransport({
    service: config.email.service,
    auth: {
        user: config.email.username,
        pass: config.email.password
    },
    from: config.email.from
});

const sendEmailPromisified = util.promisify(transporter.sendMail.bind(transporter));

export async function sendEmail(email: Email):Promise<void> {
    const mailOptions:nodemailer.SendMailOptions = {
        subject: email.subject,
        html: email.message,
        to: email.receiver
    };

    return sendEmailPromisified(mailOptions)
}