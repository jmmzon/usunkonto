import {Controller, Delete, Get, Post} from "@overnightjs/core";
import {Request, Response} from "express";

@Controller('/api/favourites')
export class FavouritesController {

    @Get('')
    private async getFavourites(req: Request, res: Response):Promise<void> {

    }

    @Post('post/:id')
    private async addPostToFavourites(req: Request, res: Response):Promise<void> {

    }

    @Delete('post/:id')
    private async removePostFromFavourites(req: Request, res: Response):Promise<void> {

    }

}