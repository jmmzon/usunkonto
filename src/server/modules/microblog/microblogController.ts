import {Controller, Delete, Get, Middleware, Post, Put} from "@overnightjs/core";
import {Request, Response} from "express";
import requireAuthorization, {getUser} from "@server/middlewares/requireAuthorization";
import {getAuthorizedPosts, getUnauthorizedPosts, MicroblogDLA} from "./microblogDLA";
import {Post as PostEntity} from "@server/models/Post";
import {validateNewPostRequest} from "./microblogValidation";

@Controller('api/microblog')
export class MicroblogController {

    /**
     * @apiDefine ApiPagination
     * @apiParam (Query) {number} take
     * @apiParam (Query) {number} skip
     */

    /**
     * @api {Get} /api/microblog/posts/hot/:period Get posts list
     * @apiName getMicroblogPosts
     * @apiGroup Microblog
     *
     * @apiUse ApiPagination
     *
     */
    @Get('posts')
    @Middleware([
        getUser
    ])
    private async getTopPosts(req: Request, res: Response): Promise<void> {
        let posts: PostEntity[];
        if(req.user) {
            posts = await getAuthorizedPosts(req.user);
        } else {
            posts = await getUnauthorizedPosts();
        }

        res.send(posts);
    }

    @Post('posts')
    @Middleware([
        requireAuthorization,
        validateNewPostRequest
    ])
    private async addPost(req: Request, res: Response): Promise<void> {
        const user = req.user;
        const post = MicroblogDLA.create()
            .setUser(req.user)
            .addNewPost(req.body);
        res.send(post);
    }

    @Put('posts/:id')
    @Middleware([
        requireAuthorization
    ])
    private async editPost(req: Request, res: Response): Promise<void> {
        const {
            user,
            params: {
                id: postID
            },
            body: postUpdateData
        } = req;

        const post = await MicroblogDLA.create().setUser(user).editPost(postID, postUpdateData);
        res.send(post);
    }

    @Delete('posts/:id')
    @Middleware([
        requireAuthorization
    ])
    private async removePost(req: Request, res: Response): Promise<void> {

    }

    @Post('posts/:id/comments')
    @Middleware([
        requireAuthorization
    ])
    private async addPostComment(req: Request, res: Response): Promise<void> {
        const {
            user,
            params: {
                id: postID
            },
            body: commentData
        } = req;

        const comment = await MicroblogDLA.create().setUser(user).addPostComment(postID, commentData);
        res.send(comment);
    }

    @Put('posts/:id/comments/:commentID')
    @Middleware([
        requireAuthorization
    ])
    private async editPostComment(req: Request, res: Response):Promise<void> {
        const {
            user,
            params: {
                commentID
            },
            body: commentData
        } = req;
        const comment = await MicroblogDLA.create().setUser(user).editPostComment(commentID, commentData);

        res.send(comment);
    }

    @Delete('posts/:id/comments')
    @Middleware([
        requireAuthorization
    ])
    private async removePostComment(req: Request, res: Response): Promise<void> {

    }


    @Put('posts/:id/vote')
    @Middleware([
        requireAuthorization
    ])
    private async postVote(req: Request, res: Response) {

    }

    @Delete('post/:id/vote')
    @Middleware([
        requireAuthorization
    ])
    private async removeVote(req: Request, res: Response): Promise<void> {

    }

    @Get('posts/upvoted')
    @Middleware([
        requireAuthorization
    ])
    private async getUpvotedPosts(req: Request, res: Response): Promise<void> {

    }


}
