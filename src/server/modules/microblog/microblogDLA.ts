import {Post, PostCommentInterface, PostInterface} from "@server/models/Post";
import {User, UserInterface} from "@server/models/User";
import {getRepository} from "@server/models";
import * as R from 'ramda';
import {NotificationsDLA} from "@server/modules/notifications/notificationsDLA";
import {
    assertPostCanBeEdited,
    assertPostIsNotAnAnswer,
    assertUserIsPostCreator
} from "@server/modules/microblog/microblogValidation";
import {getCustomRepository} from "typeorm";
import {TagRepository} from "../../repository/TagRepository";
import {UserRepository} from "../../repository/UserRepository";
import {PostRepository} from "../../repository/PostRepository";

export async function getUnauthorizedPosts() {
    const limitMax2Answers = `answers.id in (
                    select id from post postInner
                    where postInner.answerToId = post.id
                    limit 2
        )`;
    const query = getCustomRepository(PostRepository)
        .createQueryBuilder('post')
        .leftJoinAndSelect('post.answers', 'answers', limitMax2Answers)
        .where('post.answerToId is null');

    return query
        .getMany();
}

export async function getAuthorizedPosts(user: UserInterface) {
    return getUnauthorizedPosts();
}


export async function removePost(): Promise<Post> {
    return new Post();
}


export class MicroblogDLA {
    protected user: UserInterface = null;
    protected postRepository: PostRepository;
    protected userRepository: UserRepository;

    constructor() {
        this.userRepository = getCustomRepository(UserRepository);
        this.postRepository = getCustomRepository(PostRepository);
    }


    static create() {
        return new MicroblogDLA();
    }

    setUser(user: UserInterface): MicroblogDLA {
        this.user = user;
        return this;
    }

    async getPost(id: number): Promise<Post> {
        return this.postRepository.getById(id);
    }

    protected setPostData(postData: PostInterface): (post: Post) => Promise<Post> {
        return async (post: Post): Promise<Post> => {
            post.text = postData.text;
            post.user = await this.userRepository.findById(this.user.id);

            await this.setPostMentionedUsers(post, postData.mentionedUsers.map(R.prop('id')));
            await this.setPostTags(post, postData.tags.map(R.prop('name')));

            return post;
        }
    }

    async addNewPost(postData: PostInterface): Promise<Post> {
        const notificationsDLA = new NotificationsDLA();

        return this.createPost()
            .then(this.setPostData(postData))
            .then(this.savePost)
            .then(notificationsDLA.notifyUsersAboutBeingCalled)
            .then(notificationsDLA.notifyUsersAboutTagUsageInPost);
    }

    async editPost(postID: number, postData: PostInterface): Promise<Post> {
        return this.getPost(postID)
            .then(assertPostCanBeEdited)
            .then(assertUserIsPostCreator(this.user))
            .then(this.setPostData(postData))
            .then(this.savePost)
    }

    async addPostComment(postID: number, commentData: PostCommentInterface): Promise<Post> {
        const notificationsDLA = new NotificationsDLA();
        const post = await this.postRepository
            .getById(postID)
            .then(assertPostIsNotAnAnswer);


        const comment = await this.createPost()
            .then(this.setPostData(commentData))
            .then(this.setPostAsAnswerTo(post))
            .then(this.savePost)
            .then(notificationsDLA.notifyUsersAboutBeingCalled);

        await notificationsDLA.notifyUsersAboutBeingCalled(comment);
        return comment;
    }

    async editPostComment(commentID: number, commentData: PostCommentInterface): Promise<Post> {
        return this.editPost(commentID, commentData);
    }

    protected async createPost() {
        return new Post();
    }

    protected savePost = async (post: Post) => await this.postRepository.save(post);

    protected async setPostMentionedUsers(post: Post, usersIDs: number[]): Promise<Post> {
        const userRepository = await getRepository(User);
        post.mentionedUsers = await userRepository.findByIds(usersIDs);
        return post;
    }

    protected async setPostTags(post: Post, tagNames: string[]): Promise<Post> {
        const tagsRepository = getCustomRepository(TagRepository);
        post.tags = await tagsRepository.getTagsOrCreate(tagNames);
        return post;
    }

    protected setPostAsAnswerTo = (post: Post) => {
        return (comment: Post) => {
            comment.answerTo = post;
            return comment;
        }
    };

}
