import {celebrate, Joi} from "celebrate";
import {Post} from "@server/models/Post";
import {ValidationError} from "@server/exceptions/ResponseError";
import {PermissionError} from "@server/exceptions/PermissionError";
import {UserInterface} from "@server/models/User";

const User = Joi.object({
    id: Joi.number().required(),
});

const Tag = Joi.object({
    name: Joi.string().min(3).required(),
});

export const validateNewPostRequest = celebrate({
    body: {
        text: Joi.string().required(),
        mentionedUsers: Joi.array().items(User),
        tags: Joi.array().items(Tag),
    }
});

export function assertPostCanBeEdited(post: Post): Post {
    if (!post.canBeEdited()) {
        throw new ValidationError('time limit exceed, or too many answers for post');
    }

    return post;
}

export function assertUserIsPostCreator(user: UserInterface): (post: Post) => Post {
    return function (post: Post) {
        if (post.user.id !== user.id) {
            throw new PermissionError('cannot modify not own post');
        }
        return post;
    };
}

export function assertPostIsNotAnAnswer(post: Post): Post {
    if(post.answerTo) {
        throw new ValidationError('You cant add comment to a comment ;)');
    }

    return post;
}