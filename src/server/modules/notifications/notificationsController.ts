import {Controller, Get, Middleware} from "@overnightjs/core";
import {Request, Response} from "express";
import {NotificationsDLA} from "@server/modules/notifications/notificationsDLA";
import requireAuthorization from "@server/middlewares/requireAuthorization";

@Controller('api/notifications')
export class NotificationsController {
    /**
     * @api {Get} /api/notifications/tags Get list of notifications about using tag, that user subscribe
     * @apiName getTagsNotifications
     * @apiGroup Notifications
     *
     * @apiSuccess (200) {TagNotification[]} posts array of notifications
     *
     */
    @Get('tags')
    @Middleware([
        requireAuthorization
    ])
    private async getSubscribedTagsNotifications(req: Request, res: Response):Promise<void> {

    }

    /**
     * @api {Get} /api/notifications/mentions Get list of notifications user mention
     * @apiName getTagsNotifications
     * @apiGroup Notifications
     *
     * @apiSuccess (200) {UserMentionNotification[]} posts array of notifications
     *
     */
    @Get('mentions')
    @Middleware([
        requireAuthorization
    ])
    private async getMentionsNotifications(req: Request, res: Response):Promise<void> {
        const notificationsDLA = new NotificationsDLA();
        const mentions = await notificationsDLA.getMentions(req.user);

        res.send(mentions);
    }
}