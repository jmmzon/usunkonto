import {Post} from "@server/models/Post";
import * as R from 'ramda';
import {getConnection, getRepository} from "@server/models";
import {HasIDInterface} from "@server/helpers/common";
import {UserMentionNotification, UserMentionNotificationInterface} from "@server/models/UserMentionNotification";
import {getTagsSubscribers} from "@server/modules/tags/tagsDLA";
import {UserInterface} from "@server/models/User";

export interface TagSubscription {
    userId: number;
    tagId: number;
}

interface TagNotificationQueryBuilerEntity {
    receiver: HasIDInterface;
    tag: HasIDInterface;
    post: HasIDInterface;
}

export class NotificationsDLA {
    async getMentions(user: UserInterface) {
        const mentionsRepository = await getRepository(UserMentionNotification);
        return mentionsRepository.find({relations: ['mentioned', 'caller', 'post'], where: {mentioned: {id: user.id}}})
    }

    notifyUsersAboutTagUsageInPost = async (post: Post) => {
        const connection = await getConnection();
        const tagIDs = post.tags.map(R.prop('id'));

        const subscribers = await getTagsSubscribers(tagIDs);

        await connection.createQueryBuilder()
            .insert()
            .into('tag_notification')
            .values(subscribers.map(this._createNotificationRowForPost(post)))
            .execute();

        return post;
    };

    notifyUsersAboutBeingCalled = async (post: Post) => {
        const connection = await getConnection();
        const userIDs = post.mentionedUsers.map(R.prop('id'));

        await connection.createQueryBuilder()
            .insert()
            .into('user_mention_notification')
            .values(userIDs.map(this._createUserMentionNotificationForPost(post)))
            .execute();

        return post;
    };


    protected _createUserMentionNotificationForPost(post: Post): (userID: number) => UserMentionNotificationInterface {
        return function createUserMentionNotificationRow(userID: number):UserMentionNotificationInterface {
            return {
                caller: {id: post.user.id},
                mentioned: {id: userID},
                post: {id: post.id}
            }
        }
    }

    protected _createNotificationRowForPost(post: Post): (subscription: TagSubscription) => TagNotificationQueryBuilerEntity {
        return function createNotificationRow(subscription: TagSubscription): TagNotificationQueryBuilerEntity {
            return {
                receiver: {id: subscription.userId},
                tag: {id: subscription.tagId},
                post: {id: post.id}
            }
        }
    }
}

