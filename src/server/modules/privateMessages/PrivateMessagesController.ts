import {Controller, Get, Middleware, Post} from "@overnightjs/core";
import requireAuthorization from "../../middlewares/requireAuthorization";
import {
    assertFirstUserIsNotBlacklistedBySecondUser,
    assertUserHasEnabledPrivateMessages,
    validateSendMessageRequest
} from "./PrivateMessagesValidation";
import {Request, Response} from "express";
import {getPrivateMessages, getUsersByID, sendPrivateMessage} from "./PrivateMessagesDLA";
import {assertUserExists} from "../users/usersValidations";

//TODO: Dodac wyszukiwanie
//TODO: Dodac stronicowanie
@Controller('api/pw')
export class PrivateMessagesController {

    @Get('messages/:userID')
    @Middleware([
        requireAuthorization,
    ])
    private async getMessages(req: Request, res: Response) {
        const messages = await getPrivateMessages({
            id: req.user.id
        }, {
            id: req.params.userID
        });

        return res.send(messages);
    }

    @Post('message/:userID')
    @Middleware([
        requireAuthorization,
        validateSendMessageRequest
    ])
    private async sendMessage(req: Request, res: Response) {
        const receiverID: number = req.params.userID;
        const senderID: number = req.user.id;
        const {message} = req.body;

        const {
            [receiverID]: receiver,
            [senderID]: sender
        } = await getUsersByID([
            receiverID,
            senderID
        ]);

        await assertUserExists({id: req.params.userID});
        await assertFirstUserIsNotBlacklistedBySecondUser(sender, receiver);
        await assertUserHasEnabledPrivateMessages(receiver);

        const savedMessage = await sendPrivateMessage({
            receiver: {
                id: receiverID,
            },
            sender: {
                id: req.user.id
            },
            message
        });

        res.send(savedMessage);
    }

    @Post('message/:messageID/remove')
    private removeMessage() {

    }

    @Post('message/:userID/changeReadStatus')
    private changeReadStatus(req: Request, res: Response) {

    }

}