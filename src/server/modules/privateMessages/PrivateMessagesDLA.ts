import db, {getRepository} from '@server/models';
import {PrivateMessage, PrivateMessageInterface} from "@server/models/PrivateMessage";
import {User, UserInterface} from "@server/models/User";
import keyBy, {Dictionary} from "@server/libs/keyBy";

import {In} from "typeorm";

export async function getUsers(userIDs: number[], relations:string[] = []): Promise<User[]> {
    const connection = await db;
    const usersRepository = connection.getRepository(User);

    return await usersRepository.find({
        where: {
            id: In(userIDs)
        },
        relations
    });
}

export async function getUsersByID(userIDs: number[], relations: string[] = []): Promise<Dictionary<User>> {
    return keyBy(await getUsers(userIDs, relations), 'id');
}

export async function isUsersExists(userIDs: number[]) {
    const users = (await getUsers(userIDs));

    return users.length === userIDs.length;
}

export async function sendPrivateMessage(messageData: PrivateMessageInterface):Promise<PrivateMessage> {
    const connection = await db;
    const messagesRepository = connection.getRepository(PrivateMessage);

    const users = await getUsersByID([messageData.sender.id, messageData.receiver.id]);
    const message = new PrivateMessage();

    message.receiver = users[messageData.receiver.id];
    message.sender = users[messageData.sender.id];
    message.message = messageData.message;
    message.seen = false;


    return await messagesRepository.save(message);
}

export async function getPrivateMessages(firstUser: UserInterface, secondUser: UserInterface):Promise<PrivateMessage[]> {
    const privateMessageRepository = await getRepository(PrivateMessage);
    const {
        [firstUser.id]: firstDBUser,
        [secondUser.id]: secondDBUser
    } = await getUsersByID([firstUser.id, secondUser.id]);

    return await privateMessageRepository.find({
        where: [
            {receiver: firstDBUser, sender:secondDBUser},
            {receiver: secondDBUser, sender: firstDBUser}
        ],
        loadRelationIds: true
    });
}

