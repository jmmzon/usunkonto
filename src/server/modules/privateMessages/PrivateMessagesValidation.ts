import {User} from "@server/models/User";
import {celebrate, Joi} from "celebrate";
import {hasSecondUsersBlacklistedFirstUser} from "../blacklist/BlacklistDLA";
import BlacklistError from "@server/exceptions/BlacklistError";
import {ValidationError} from "@server/exceptions/ResponseError";

export const validateSendMessageRequest = celebrate({
    body: {
        message: Joi.string().required()
    },
    params: {
        userID: Joi.number().required()
    }
},);

export async function assertFirstUserIsNotBlacklistedBySecondUser(firstUser: User, secondUser:User):Promise<void> {
    if(await hasSecondUsersBlacklistedFirstUser(firstUser, secondUser)) {
        throw new BlacklistError();
    }
}

export async function assertUserHasEnabledPrivateMessages(user: User) {
    if(!await user.getSettingValue('ALLOW_PRIVATE_MESSAGES')) {
        throw new ValidationError('User has disabled private messages');
    }
}