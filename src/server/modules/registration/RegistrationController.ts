import {Request, Response} from 'express';
import {Controller, Get, Middleware, Post} from "@overnightjs/core";

import {
    assertEmailsIsExist,
    assertEmailsIsNotExist,
    assertUsernameIsNotExist,
    validateActivateRequestData,
    validateCheckEmailAvailability,
    validateCheckUsernameAvailability,
    validateCreateUserRequestData
} from "./RegistrationValidations";
import {activateUserAccount, createNewUser, isUsernameExists} from "./RegistrationDLA";
import {sendEmail} from "@server/modules/emails";


@Controller('api/registration')
export class RegistrationController {
    @Get('x')
    private async getRegistrationIndex(req: Request, res: Response) {
        await assertUsernameIsNotExist(req.body.username);
    }

    /**
     * @apiDefine User
     * @apiSuccess (200) {number} id id of created user
     * @apiSuccess (200) {String} firstName first name of user
     * @apiSuccess (200) {String} lastName last name of user
     * @apiSuccess (200) {String} username/login username of user
     */

    /**
     * @api {post} /api/registration create new user
     * @apiName RegisterNewUser
     * @apiGroup Registration
     *
     * @apiParam (Body) {String} email Email of new created user. At this mail, account activation email will be send
     * @apiParam (Body) {String} username Username/Login for new user. Minimum length: 3
     * @apiParam (Body) {String} password Password for new user. Minimum length: 6
     * @apiParam (Body) {String} [firstName] Optional first name of user
     * @apiParam (Body) {String} [lastName] Optional last name of user
     *
     * @apiUse User
     *
     * @apiError (400) UsernameIsAlreadyTaken Username is already taken
     * @apiError (400) EmailsAlreadyTaken Email is already taken
     *
     */
    @Post()
    @Middleware([
        validateCreateUserRequestData
    ])
    private async createAccount(req: Request, res: Response): Promise<void> {
        await assertUsernameIsNotExist(req.body.username);
        await assertEmailsIsNotExist(req.body.email);

        const {activationCode, user} = await createNewUser(req.body);

        await sendEmail({
            message: `Your activation code is:  ${activationCode.activationCode}`,
            subject: 'Account created',
            receiver: user.email
        });

        res.send();
    }


    /**
     * @api {get} /api/registration/activate Activate user account
     * @apiName ActivateUserAccount
     * @apiGroup Registration
     *
     * @apiParams (Body) {string} email email of user
     * @apiParams (Body) {string} activationCode activation code from email
     *
     * @apiSuccess (200) AcountActivated
     *
     * @apiError (400) Error
     *
     */
    @Get('activate')
    @Middleware(validateActivateRequestData)
    private async postActivateAccount(req: Request, res: Response): Promise<void> {
        await assertEmailsIsExist(req.query.email);
        await activateUserAccount(req.query);

        res.send();
    }


    /**
     * @api {get} /api/registration/avail/email?email= check if username/login is available for registration
     * @apiName CheckUsernameAvailability
     * @apiGroup Registration
     * @apiParam (Query) {String} username
     */
    @Get('avail/username')
    @Middleware(validateCheckUsernameAvailability)
    private async checkIfUsernameIsAvailable(req: Request, res: Response): Promise<void> {
        const username: string = req.query.username;
        res.send(!await isUsernameExists(username));
    }


    /**
     * @api {get} /api/registration/avail/email?email= check if email is available for registration
     * @apiName CheckEmailAvailability
     * @apiGroup Registration
     * @apiParam (Query) {String} email
     *
     * @apiSuccess (200) {boolean} true email is avail
     * @apiError (409) {boolean} false email is not avail
     */

    @Get('avail/email')
    @Middleware(validateCheckEmailAvailability)
    private async checkIfEmailIsAvailable(req: Request, res: Response): Promise<void> {
        const email: string = req.query.email;
        const userNameExist = await isUsernameExists(email);

        if (userNameExist) {
            res.status(409).send(false);
        } else {
            res.status(200).send(true);
        }
    }
}
