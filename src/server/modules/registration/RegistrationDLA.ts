import db, {getRepository} from '../../models';
import {User, UserInterface, UserStatus} from '@server/models/User';
import {UserAccountActivation} from "@server/models/UserAccountActivation";
import {ActivationData} from "./types";
import {ValidationError} from "@server/exceptions/ResponseError";
import {NotFoundError} from "@server/exceptions/NotFoundError";
import randomString from "@server/helpers/randomString";

export async function isUsernameExists(username: string): Promise<boolean> {
    const connection = await db;

    const userRepository = connection.getRepository(User);
    return await userRepository.count({username}) > 0;
}

export async function isEmailExists(email: string): Promise<boolean> {
    const connection = await db;

    const userRepository = connection.getRepository(User);
    return await userRepository.count({email}) > 0;
}

export async function createNewUser(userData: UserInterface) {
    const connection = await db;
    const userRepository = connection.getRepository(User);
    const userAccountActivationRepository = connection.getRepository(UserAccountActivation);


    const user = new User();
    const activationCode = new UserAccountActivation();

    user.username = userData.username;
    user.email = userData.email;
    user.firstName = userData.firstName;
    user.lastName = userData.lastName;

    await user.setPassword(userData.password);
    user.setStatus(UserStatus.INACTIVE);

    activationCode.activationCode = randomString(8);
    activationCode.user = user;

    await userRepository.save(user);
    await userAccountActivationRepository.save(activationCode);

    return {
        activationCode,
        user
    }
}

export async function assertActivationDataIsValid(activationData: ActivationData):Promise<UserAccountActivation> {
    const connection = await db;
    const userRepository = connection.getRepository(User);

    const user = await userRepository.findOne({
        where: {
            email: activationData.email,
        }
    });

    if(!user) {
        throw new NotFoundError('User does not exist');
    }

    const activationCode = await user.activationCode;

    if(!activationCode)  {
        throw new ValidationError('Account has been already activated');
    }

    if (activationCode.activationCode !== activationData.activationCode) {
        throw new ValidationError('Invalid activation code');
    }

    return activationCode;
}

export async function activateUserAccount(activationData: ActivationData):Promise<void> {
    const userAccountActivationRepository = await getRepository(UserAccountActivation);
    const userRepository = await getRepository(User);

    const activationCode = await assertActivationDataIsValid(activationData);
    const user = await userRepository.findOne({where: {email: activationData.email}});

    user.setStatus(UserStatus.ACTIVE);

    await userAccountActivationRepository.remove(activationCode);
    await userRepository.save(user);

}