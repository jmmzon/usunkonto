import {isEmailExists, isUsernameExists} from "./RegistrationDLA";
import {ValidationError} from "@server/exceptions/ResponseError";
import {celebrate, Joi} from "celebrate";


export const validateCreateUserRequestData = celebrate({
    body: {
        email: Joi.string().email().required(),
        username: Joi.string().required().min(3),
        password: Joi.string().required().min(6),
        firstName: Joi.string().optional(),
        lastName: Joi.string().optional()
    }
});

export const validateCheckUsernameAvailability = celebrate({
    query: {
        username: Joi.string().required().min(3)
    }
});


export const validateCheckEmailAvailability = celebrate({
    query: {
        email: Joi.string().email().required()
    }
});

export const validateActivateRequestData = celebrate({
    query: {
        email: Joi.string().email().required(),
        activationCode: Joi.string().length(16).required()
    }
});

export async function assertUsernameIsNotExist(username: string): Promise<void> {
    if (await isUsernameExists(username)) {
        throw new ValidationError(400, 'Username is already taken');
    }
}

export async function assertEmailsIsNotExist(email: string): Promise<void> {
    if (await isEmailExists(email)) {
        throw new ValidationError(400, 'Email is already taken');
    }
}

export async function assertEmailsIsExist(email: string): Promise<void> {
    if (!await isEmailExists(email)) {
        throw new ValidationError(400, 'User does not exist');
    }
}