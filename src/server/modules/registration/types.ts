export interface ActivationData {
    email: string;
    activationCode: string;
}