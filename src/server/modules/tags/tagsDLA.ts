import {getConnection} from "@server/models";

import {TagSubscription} from "@server/modules/notifications/notificationsDLA";


export async function getTagsSubscribers(tagsIDs: number[]): Promise<TagSubscription[]> {
    const connection = await getConnection();

    return await connection.createQueryBuilder()
        .from('user_observed_tags_tag', 'subscription')
        .where('subscription.tagId IN (:...tagsIDs)', {tagsIDs: tagsIDs})
        .getRawMany();
}