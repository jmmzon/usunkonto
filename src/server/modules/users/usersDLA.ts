import {User, UserInterface} from "@server/models/User";
import {getRepository} from "@server/models";

export async function isUserExists(user: UserInterface) {
    const userRepository = await getRepository(User);

    return await userRepository.count({
        where: {
            id: user.id
        }
    });
}