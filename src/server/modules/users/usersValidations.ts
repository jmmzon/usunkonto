import {User, UserInterface} from "@server/models/User";
import {isUserExists} from "./usersDLA";
import {NotFoundError} from "@server/exceptions/NotFoundError";

export async function assertUserExists(user: UserInterface):Promise<void> {
    if(! await isUserExists(user)) {
        throw new NotFoundError(`User with id ${user.id} does not exist`);
    }
}

export async function assertUserIsNotBanned(user: User) {
    // if
}