import {EntityRepository, Repository} from "typeorm";
import {Post} from "@server/models/Post";
import {NotFoundError} from "@server/exceptions/NotFoundError";

@EntityRepository(Post)
export class PostRepository extends Repository<Post>{
    async getById(id: number):Promise<Post> {
        const post = await this.findOne({
            where: {
                id
            },
            relations: ['answerTo']
        });

        if(!post) {
            throw new NotFoundError('Post not found');
        }

        return post;
    }
}