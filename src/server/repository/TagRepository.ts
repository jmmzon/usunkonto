import {EntityRepository, In, Repository} from "typeorm";
import {Tag} from "@server/models/Tag";
import * as R from 'ramda';

@EntityRepository(Tag)
export class TagRepository extends Repository<Tag> {
    async getTagsByNames(tagNames: string[]): Promise<Tag[]> {
        return this.find({
            where: {
                name: In(tagNames)
            }
        })
    }

    async createTags(tagNames: string[]): Promise<Tag[]> {
        if (!tagNames.length) {
            return [];
        }

        const {generatedMaps} = await this.createQueryBuilder()
            .insert()
            .into(Tag)
            .values(tagNames.map(R.objOf('name')))
            .execute();

        return generatedMaps as Tag[];
    }

    async getTagsOrCreate(tagNames: string[]): Promise<Tag[]> {
        const existingsTags: Tag[] = await this.getTagsByNames(tagNames);

        const existingTagNames = existingsTags.map(R.prop('name'));
        const notExistingTags = R.difference(tagNames, existingTagNames);

        const createdTags = await this.createTags(notExistingTags);

        return existingsTags.concat(createdTags);
    }
}