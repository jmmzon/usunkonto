import {EntityRepository, Repository} from "typeorm";
import {User} from "@server/models/User";

@EntityRepository(User)
export class UserRepository extends Repository<User>{
    async findById(id: number) {
        const [user] = await this.findByIds([id]);
        return user;
    }
}